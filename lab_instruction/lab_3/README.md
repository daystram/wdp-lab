# Lab 3: PostgreSQL _Setting_ in Heroku, introduction to _models_ and TDD

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## LAB 3 Objectives

What should you learn from codes & documentation in this sub-directory?

- Activating PostgreSQL in Heroku 
- Understand how to use _models_ in _Django Project_ 
- TDD discipline in _Web Application_



## Checklist

1. Every URLs `/lab-2/`, `/lab-2-addon/`, and `/lab-3/` should have _Navigation Bar_
    1. [ ] I have located a file named `base.html` in _folder_ `_Root Folder_/_templates_`
    2. [ ] I've already modify my views.py in every URLs given, to use `index_lab2.html` as my HTML template
    3. [ ] I've already modify my views.py in every URLs given, to use `description_lab2addon.html` as my HTML template 
2. Create new feature **Menulis Kegiatan** and **Menampilkan Semua Kegiatan** in page _Diary_ :
    1. [ ] I've created new _application_ named `lab_3`
    2. [ ] I've registered my `lab_3` in the Django settings as _INSTALLED_APPS_
    3. [ ] I've implemented _Test Case_ in `lab_3/tests.py`
    4. [ ] I've implemented `lab_3/views.py`
    5. [ ] I've configured my `lab_3/urls.py`
    6. [ ] I've configured my `praktikum/urls.py` in order to get my `lab_3/urls.py` accessible
3. Make sure that you have good _Code Coverage_: 
    1. [ ] If you haven't configure Gitlab to show your _Code Coverage_, please read WIKI Page [Show Code Coverage in Gitlab](https://gitlab.com/PPW-2017/ppw-lab-ki/wikis/show-code-coverage)
    2. [ ] I've reached 100% _Code Coverage_

## Challenge Checklist

Just do one of them:
1. [ ] I've fix my _Website_ colour & layout 
2. [ ] Create an _Input Validation_ as a Client Side Programming. It will deny value submission whenever the date entry is incorrect.
(Saat ini yang dilakukan oleh program adalah memberikan _stacktrace error_. Hal ini biasanya terjadi di _browser_ Mozilla)
Validasi perlu dilakukan selain di browser (HTML5 atau Java-Script) dan
3. [ ] Create an _Input Validation_ as a Server Side Programming. It will deny value submission by raising an exception.
It's one of _best-practices_ to avoid _injection_/(_hacking activity_)



## Adding PostgreSQL _Addon_ PostgreSQL in Heroku

First, you should open Heroku _command prompt_ by installing (heroku-cli) in your local environment. You can follow the instructions in [https://devcenter.heroku.com/articles/heroku-cli](https://devcenter.heroku.com/articles/heroku-cli).

After _heroku-cli_ installation, open your _terminal_ (in Mac/Linux) or _command prompt_ (using _cmd_ in Windows), and type `heroku` to make sure that you have successfully install _heroku-cli_.

Heroku will create a PostgreSQL _Database_ for every installed application in Heroku. You can use your own database for the rest of your application's life.

To check your own _database_ in Heroku, execute this command :

> heroku addons

You can see examples from this URL [https://devcenter.heroku.com/articles/heroku-postgresql#provisioning-heroku-postgres](https://devcenter.heroku.com/articles/heroku-postgresql#provisioning-heroku-postgres).

If Heroku didn't automatically create your database, then you have to create the _Database_ manually by execute this command:

> heroku addons:create heroku-postgresql:hobby-dev --app <YOURAPPNAME>

## Django _Models_ dan _Migrations_ 

In Django, _Model_ is the source of informations for your Application. It contains the definition (and characteristics) of your information, and also the data stored. The definition consists of column name; data type; etc. Generally, every _Model_ in Django will be represented as a _Table_ in your _database_. Django will automatically create file `models.py` for every Django `app`.

> To know more about Django _Model_, please go to this [https://docs.djangoproject.com/en/1.11/topics/db/models/](https://docs.djangoproject.com/en/1.11/topics/db/models/).

_Migrations_ is an Django internal procedure to apply changes in _Models_. Whenever you add/delete/change a variable in `models.py`, you need to do _Migrations_ to apply your changes in your PostgreSQL _Database table_.
There are 2 commands in _Migrations_: `migrate` and `makemigrations` .

> In every Django `app`, you will find _migrations_ direktori. Django will automatically populates this directory after you successfully execute the _migations_ process.

`makemigrations` is a command to track changes between your previous and new/changed _models_. It will track your `models.py` and comparing with the previous model in _migrations_ directory.

`migrate` is a command to apply changes to your PostgreSQL _Database_. 

> Further explanation on Django _Migrations_ please go to this URL [https://docs.djangoproject.com/en/1.11/topics/migrations/] (https://docs.djangoproject.com/en/1.11/topics/migrations/)

## Changing _Landing Page_ by adding _Navigation Bar_

1. _Replace file_  `lab_2_addon/description_lab2addon.html` with  `templates/description_lab2addon.html` in _root folder_
2. Run Django locally:

    > python manage.py runserver 8000 

Voila, you can see _Navigation Bar_ in your _Landing Page_.

## Create Page _Diary_ (Using TDD Discipline)

1. Please download new `requirement.txt` [file](https://gitlab.com/PPW-2017/ppw-lab-ki/blob/master/requirements.txt) from PPW LAB KI repository, and replace your `requirement.txt`.
2. Run your _Virtual Environment_
3. Install required module in `requirement.txt`
    ```python
    pip install -r requirement.txt
    ```
4. Create new apps named `lab_3`
5. Move HTML file `templates/to_do_list.html` into `lab_3/templates` folder

    > Please create the_folder_ `lab_3/templates` if you didn't have it yet.

6. Register your `lab_3` as INSTALLED_APPS in `praktikum/settings.py`
7. Modify your LAB 3 _Test Case_ in  `lab_3/tests.py` file
    ```python
        from django.test import TestCase, Client
        from django.urls import resolve
        from .views import index

        class Lab3Test(TestCase):
            def test_lab_3_url_is_exist(self):
                response = Client().get('/lab-3/')
                self.assertEqual(response.status_code,200)

            def test_lab_3_using_to_do_list_template(self):
                response = Client().get('/lab-3/')
                self.assertTemplateUsed(response, 'to_do_list.html')

            def test_lab_3_using_index_func(self):
                found = resolve('/lab-3/')
                self.assertEqual(found.func, index)
    ```
    It's a _Test Case_ to make sure that the URL `<YOURHOSTNAME>/lab-3/` accessible, and your LAB 3 is using `to_do_list.html` template.

8. If you run _test_ localy, you will see _error_. Please execute:

    > python manage.py test

9. To solve every _Test Case_, first think to do is you have to configure _URL routing_ in `lab_3`. Please create a file `lab_3/urls.py` and fill with this code:
    ```python
        from django.conf.urls import url
        from .views import index
        #url for app
        urlpatterns = [
            url(r'^$', index, name='index'),
        ]
    ```
10. Insert code in `lab_3/views.py` to show `to_do_list.html`:
    ```python
        from django.shortcuts import render
        # Create your views here.
        diary_dict = {}
        def index(request):
            return render(request, 'to_do_list.html', {'diary_dict' : diary_dict})
    ```
11. Add code `url(r'^lab-3/', include(lab_3,namespace='lab-3')),` in `praktikum/urls.py` to enable `lab_3` application:
    ```python
        ...........
        import lab_3.urls as lab_3
        ...........

        urlpatterns = [
            .............
            url(r'^lab-3/', include(lab_3,namespace='lab-3')),
        ]
    ```
    `.....` means, it's your existing configuration, please 

12. Please rerun your _test_ and make sure that you have already solve every _Test Case_

    > `python manage.py test`
    
13. To create activity list feature, first is to create a new **models**. Before creating your **models**, you have to follow TDD discipline. Create your Test, by inserting _Test Case_ in `lab_3/tests.py` :
    ```python
        .........
        from .models import Diary
        from django.utils import timezone

        class Lab3Test(TestCase):
            ......................
            def test_model_can_create_new_activity(self):
                #Creating a new activity
                new_activity = Diary.objects.create(date=timezone.now(),activity='Aku mau latihan ngoding deh')

                #Retrieving all available activity
                counting_all_available_activity = Diary.objects.all().count()
                self.assertEqual(counting_all_available_activity,1)
    ```
14. Run your _Test Case_, you will see _Error_ in the _Test Case_

    > Trivia
    >
    > This will be your TDD discipline. In order to create new feature, first thing to do is creating your _Test Case_.
    When you run the _Test Case_, it should shows _Error_ (RED). After that, you are allowed to implement your new feature. 
    You will know that you have finished your implementation by getting (GREEN) checklist whenever your _Test Case_ successfull.
    
15. Please create a **models** named `Diary` in `lab_3/models.py`:
    ```python
        from django.db import models

        # Create your models here.
        class Diary(models.Model):
            date = models.DateTimeField()
            activity = models.TextField(max_length=60)
    ```

16. Run your _Test Case_ (`python manage.py test`). You will see _Error_ in your _Test Case_

    > Why?
    >
    > It's because you haven't do _make migrations_ and _migrate_ process
    >
    > Run command `python manage.py makemigrations` and `python manage.py migrate` to track changes & apply to PostgreSQL Database

17. Now, please re-open your `lab_3/tests.py` add add some code:
    ```python
        ........
        class Lab3Test(TestCase):
            ........
            def test_can_save_a_POST_request(self):
                response = self.client.post('/lab-3/add_activity/', data={'date': '2017-10-12T14:14', 'activity' : 'Maen Dota Kayaknya Enak'})
                counting_all_available_activity = Diary.objects.all().count()
                self.assertEqual(counting_all_available_activity, 1)

                self.assertEqual(response.status_code, 302)
                self.assertEqual(response['location'], '/lab-3/')

                new_response = self.client.get('/lab-3/')
                html_response = new_response.content.decode('utf8')
                self.assertIn('Maen Dota Kayaknya Enak', html_response)
    ```
    This code will test whether you've already do the Diary feature

18. Re-run _Test Case_ ( `python manage.py test` ), it will shows _error_.
     > This is just a chunk of _error_
     > from .views import index, add_activity
     > 
     > ImportError: cannot import name 'add_activity'

19. Open your `lab_3/views.py` and add code:
    ```python
    from .models import Diary
    from datetime import datetime
    import pytz
    ....
    def add_activity(request):
        if request.method == 'POST':
            date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
            Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=request.POST['activity'])
            return redirect('/lab-3/')
    ```
20. Add code in `lab_3/urls.py`:
     ```python
    ...
    from .views import add_activity
    urlpatterns = [
        ...
        url(r'add_activity/$', add_activity, name='add_activity'),
    ]
     ```
    It will make sure that the URL `<YOURHOSTNAME>/lab-3/add_activity` is accessible from your browser

21. Re-run the _Test Case_, and you will find _Error_

    >The test will fail. You've implement the _add_activity_ function to store data to _Database_, but you haven't show the stored
    data to web page. Therefore, you have to implement a page to display the data that have already stored in _Database_

22. To show/display data in _Database_ change the code in `lab_3/views.py`:

    ```python
    from django.shortcuts import render, redirect
    from .models import Diary
    from datetime import datetime
    import pytz
    import json
    # Create your views here.
    diary_dict = {}
    def index(request):
        diary_dict = Diary.objects.all().values()
        return render(request, 'to_do_list.html', {'diary_dict' : convert_queryset_into_json(diary_dict)})
    
    def add_activity(request):
        if request.method == 'POST':
            date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
            Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=request.POST['activity'])
            return redirect('/lab-3/')
    
    def convert_queryset_into_json(queryset):
        ret_val = []
        for data in queryset:
            ret_val.append(data)
        return ret_val
    ```

23. Run the _Test Case_ and the test should show **passed**
    > ....................
    >
    > Ran 19 Test Ok
    As Gandalf in Lord of The Ring said: "You Should Pass" :)

